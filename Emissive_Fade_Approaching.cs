﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emissive_Fade_Approaching : MonoBehaviour {

    private Color initialEmissionColor;
    private Color displayedColor;
    private float timeRandom;
    private bool emissiveUpdate = false ;
    private Vector3 playerpos;
    private float triggerRadius;
    private float progression = 0.0f;

    // Use this for initialization
    void Start ()
    {
        initialEmissionColor = GetComponent<Renderer>().material.GetColor("_EmissionColor");
        displayedColor = initialEmissionColor * 0;
        //GetComponent<Renderer>().material.SetColor("_EmissionColor",newColor);
        SetEmissiveColor(displayedColor);
        timeRandom = Random.Range(0.0f, 5.0f);
        //timeRandom = 0.0f;
        triggerRadius = GetComponent<SphereCollider>().radius;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (emissiveUpdate)
        {
            playerpos = GameObject.FindGameObjectWithTag("MainCamera").transform.position;
            progression = Mathf.Clamp01( 1- Vector3.Distance(gameObject.transform.position, playerpos)/(triggerRadius * transform.localScale.y));
            float floor = 0.0f;
            float ceiling = 1.0f;
            displayedColor = initialEmissionColor * Mathf.PingPong(progression, ceiling - floor);
            print(progression);
            SetEmissiveColor(displayedColor);
        }
    }

    void SetEmissiveColor (Color newColor)
    {
        GetComponent<Renderer>().material.SetColor("_EmissionColor", newColor);
        DynamicGI.SetEmissive(GetComponent<Renderer>(), newColor);
    }

    void OnTriggerEnter()
    {
        emissiveUpdate = true;
    }

    void OnTriggerExit()
    {
        emissiveUpdate = false;
    }
}
